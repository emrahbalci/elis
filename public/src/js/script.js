import jQuery from 'jquery';
import 'jquery-validation';
import 'jquery-colorbox';
import common from './utils/common';
import 'owl.carousel';
{

   //mobile menu
    common.mobileMenu();

    //toggle
    common.initMap();

    //form field label animate
    common.formLabelAnimate();

    //form validation
    // common.formValidation();

    //tab
    common.tab();

    let homepageSlider = $('.homepage-slider').owlCarousel({
        loop: false,
        nav: true,
        dots: true,
        items: 1,
        dotsContainer: '.homepage-slider-nav'
    });

    $('.homepage-slider-nav ').on('click', '.item', function(e) {
        homepageSlider.trigger('to.owl.carousel', [$(this).index(), 300]);
    });


    $('.references-slider').owlCarousel({
        loop: true,
        dots: false,
        nav: true,
        margin: 0,
        lazyLoad:true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                nav: true,
                dots: false
            },
            500: {
                items: 3,
            },
            992: {
                items: 4,
            }
        }
    });

    $('.services-slider').owlCarousel({
        center: true,
        items:2,
        loop:false,
        lazyLoad:true,
        lazyLoadEager: 2,
        nav: true,
        margin:10
    });

    $(".colorbox").colorbox({maxHeight: '100%', maxWidth: '100%'});
}
