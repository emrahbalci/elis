let modules = {}

modules.mobileMenu = () => {
    $(document).on('click', '.toggle-menu', function() {
        $(this).toggleClass('active');
        $('.main-menu').toggleClass('active');
    })
};

modules.initMap = () => {
    let mymap = L.map('maps').setView([41.084629, 29.045857], 17);
    L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        subdomains: 'abcd',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'your.mapbox.access.token',
    }).addTo(mymap);

    let greenIcon = L.icon({
        iconUrl:  'public/dist/img/marker.png',

        iconSize: [58, 72], // size of the shadow
        iconAnchor: [30, 72], // point of the icon which will correspond to marker's location
        shadowAnchor: [50, 62] // point from which the popup should open relative to the iconAnchor
    });
    L.marker([41.084629, 29.045857], {icon: greenIcon}).addTo(mymap);
};

modules.formLabelAnimate = () => {
    $('.form-field input, .form-field textarea').on('keyup keydown', function () {
        if ($(this).val().length > 0)
            $(this).addClass('filled')
        else
            $(this).removeClass('filled')

    })
};

modules.formValidation = () => {
};

modules.tab = () => {
    let $tabs = $('.tab-button'),
        $tabContent = $('.services-slider');

    if ($tabs.length > 0) {
        $tabs.on('click', function (e) {
            e.preventDefault();
            $(this).addClass('active').siblings().removeClass('active');
            $tabContent.removeClass('active').siblings($(this).attr('href')).addClass('active');
        });
        $tabs.eq(0).click();
    }
}


module.exports = modules
