const { task, parallel, src, dest, watch, lastRun, series } = require('gulp');
const imagemin = require('gulp-imagemin'),
    sass = require("gulp-sass"),
    pug = require("gulp-pug"),
    concat = require("gulp-concat"),
    rollup = require('gulp-better-rollup'),
    babel = require('rollup-plugin-babel'),
    commonjs = require('@rollup/plugin-commonjs'),
    resolve = require('rollup-plugin-node-resolve'),
    browserSync = require('browser-sync'),
    uglify = require("rollup-plugin-uglify"),
    iconfont = require('gulp-iconfont'),
    consolidate = require('gulp-consolidate');


const bundlePath = "output/public/dist/";
const jsPath = 'public/src/js/script.js';
const jsPluginPath = 'public/src/js/plugins/*.js';
const cssPath = 'public/src/scss/style.scss';
const imgPath = 'public/src/img/**/**';
const fontsPath = 'public/src/fonts/**/**';
const viewsPath = 'views/**.pug';
const iconsPath = 'public/src/icons/*.svg';

async function js() {
    return src(jsPath)
        .pipe(rollup(
            {
                plugins: [
                    babel({
                        babelrc: false,
                        presets: [['@babel/preset-env', { modules: false }]],
                    }),
                    resolve({ browser: true }),
                    commonjs()
                ]
            },
            { format: 'umd' }))
        .pipe(concat('bundle.js'))
        .pipe(dest(bundlePath + 'js/'))
        .pipe(browserSync.reload({ stream: true }))
}
function jsPlugins() {
    return src(jsPluginPath)
        .pipe(concat('plugins.js'))
        .pipe(dest(bundlePath + 'js/'))
        .pipe(browserSync.reload({ stream: true }))
}
function css() {
    return src(cssPath)
        .pipe(sass({ outputStyle: "compressed" }))
        .pipe(dest(bundlePath + 'css/'))
        .pipe(browserSync.reload({ stream: true }))
}

function images() {
    return src(imgPath, { since: lastRun(images), allowEmpty: true })
        .pipe(imagemin())
        .pipe(dest(bundlePath + 'img/'));
}

function webFonts(done) {
    let runTimestamp = Math.round(Date.now() / 1000);
    return src(iconsPath)
        .pipe(iconfont({
            fontName: 'icons', // required
            prependUnicode: false, // recommended option
            appendCodepoints: true,
            formats: ['ttf', 'eot', 'woff', 'svg'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp, // recommended to get consistent builds when watching files
            normalize: true,
            fontHeight: 448, // matching IcoMoon's defaults for the font-awesome icons @ "14px grid"
            descent: 64
        }))
        .on('glyphs', (glyphs) => {
            const options = {
                className: 'icon',
                fontName: 'icons',
                fontPath: '../fonts/web-icons/', // set path to font (from your CSS file if relative)
                glyphs: glyphs.map(mapGlyphs)
            }
            src(`public/src/icons/webicons.css`)
                .pipe(consolidate('lodash', options))
                .pipe(dest(bundlePath + 'css/')) // set path to export your CSS
        })
        .pipe(dest(bundlePath + 'fonts/web-icons/'));

}

function mapGlyphs(glyph) {
    return { name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0) }
}

function fonts() {
    return src(fontsPath, { allowEmpty: true })
        .pipe(dest(bundlePath + 'fonts/'))
}

function views() {
    return src(viewsPath)
        .pipe(pug())
        .pipe(dest('output/'))
        .pipe(browserSync.reload({ stream: true }))
}

function browserSyncBrowser() {
    browserSync.init({
        server: {
            baseDir: "./",
            index: "./output/index.html"
        }
    })
};

function bsReload() {
    browserSync.reload();
};

exports.build = () => parallel(views, js, jsPlugins, css, images, fonts, webFonts)();

exports.default = series(js, jsPlugins, css, views, fonts, webFonts, (done) => {
    watch([jsPath, 'public/src/js/**/**'], js);
    watch([cssPath, 'public/src/scss/**/**'], css);
    watch(imgPath, images);
    watch(fontsPath, fonts);
    watch(iconsPath, webFonts);
    watch([viewsPath, 'views/**/**'], views);
    browserSyncBrowser();
    done();
});
